# computer-graphics-2018-19

Laboratory exercises for the subject of Computer Graphics on Faculty of Electrical Engineering, Mechanical Engineering and Naval Architecture at Split, Croatia. 

Academic year 2018/19.

The exercises are being done in OpenGL in C# Windows Forms projects in Visual Studio 2017.

The repo will be organized as follows: 

1. master branch is used as a wireframe for all other exercises

2. each exercise has its own branch (ex. exercise 1 on branch exercise-1)

## Instructions

### Step 1

To use the repo fork it into your own account and then git clone into local working directory. Any changes or pull requests to this repo **will not be accepted.**

### Step 2

Open Visual Studio and open the solution located in:

`computer-graphics-2018-19\exercise\exercise.sln`

As already mentioned above, master branch is the wireframe for all exercises, you should *always checkout from master when creating a new branch*. Other branches follow a naming convention: **exercise-number**.