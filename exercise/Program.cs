﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CsGL.OpenGL;

namespace exercise
{
    public class OurView : OpenGLControl
    {
        public OurView()
        {
            this.KeyDown += new KeyEventHandler(OurView_OnKeyDown);
        }

        protected void OurView_OnKeyDown(object Sender, KeyEventArgs key)
        {
            switch (key.KeyCode)
            {
                case Keys.Escape:
                    Application.Exit();
                    break;
            }

            Refresh();
        }

        // Method that draws on screen.
        public override void glDraw()
        {
            GL.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
            GL.glLoadIdentity();
        }

        protected override void InitGLContext()
        {
            GL.glShadeModel(GL.GL_SMOOTH);
            GL.glClearColor(0.0f, 0.0f, 0.0f, 0.5f);
            GL.glClearDepth(1.0f);
            GL.glEnable(GL.GL_DEPTH_TEST);
            GL.glDepthFunc(GL.GL_LEQUAL);
            GL.glHint(GL.GL_PERSPECTIVE_CORRECTION_HINT, GL.GL_NICEST);
            
            Size s = this.Size;
            GL.glMatrixMode(GL.GL_PROJECTION);
            GL.glLoadIdentity();
            GL.gluPerspective(45f, (double)s.Width / (double)s.Height, 0.1f, 100.0f);
            GL.glMatrixMode(GL.GL_MODELVIEW);
        }
    }

    public class MainForm : Form
    {
        private OurView view;

        public MainForm()
        {
            this.AutoScaleBaseSize = new Size(5, 13);
            this.ClientSize = new Size(740, 580);
            this.Name = "MainForm";
            this.Text = "Vjezba";
            this.view = new OurView();
            this.view.Parent = this;
            this.view.Dock = DockStyle.Fill;
        }

        static void Main()
        {
            Application.Run(new MainForm());
        }
}
}
